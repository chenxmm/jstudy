package sprout.jstudy.solr.domain.user;

import org.apache.solr.client.solrj.beans.Field;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 14-3-20
 * Time: 下午3:20
 * To change this template use File | Settings | File Templates.
 */
public class User {

    @Field
    private long id;
    @Field
    private String name;
    @Field
    private int sex;
    @Field
    private int age;
    @Field
    private String mobile;
    @Field
    private String address;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", sex=" + sex +
                ", age=" + age +
                ", mobile='" + mobile + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
