package sprout.jstudy.mongodb.dao.user;

import sprout.jstudy.mongodb.domain.user.User;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: sprout
 * Date: 12-12-3
 * Time: 上午11:24
 * To change this template use File | Settings | File Templates.
 */
public interface UserDao {

    public int add(User user);

    public int delete(User user);

    public int update(User user);

    public User getById(String id);

    public List<User> find();

}
