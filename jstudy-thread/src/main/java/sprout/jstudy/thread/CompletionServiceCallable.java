package sprout.jstudy.thread;

import java.util.Random;
import java.util.concurrent.Callable;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 14-7-19
 * Time: 下午4:13
 * To change this template use File | Settings | File Templates.
 */
public class CompletionServiceCallable implements Callable<Long> {

    private String name;

    public CompletionServiceCallable(String name) {
        this.name = name;
    }

    @Override
    public Long call() throws Exception {
        System.out.println(String.format("[%s]线程开始工作", name));
        long time = new Random().nextInt(5) * 1000;
        Thread.sleep(time);
        System.out.println(String.format("[%s]线程结束工作，共执行%d毫秒", name, time));
        return time;
    }

}
