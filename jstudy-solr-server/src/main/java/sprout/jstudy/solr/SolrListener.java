package sprout.jstudy.solr;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.File;
import java.net.URI;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 14-8-1
 * Time: 下午5:15
 * To change this template use File | Settings | File Templates.
 */
public class SolrListener implements ServletContextListener {

    //solr配置文件目录
    private static final String SOLR_HOME = "/solr";
    private static final String SOLR_SOLR_HOME_KEY = "solr.solr.home";

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        try {
            URI uri = Thread.currentThread().getContextClassLoader().getResource(SOLR_HOME).toURI();
            File file = new File(uri);
            System.setProperty(SOLR_SOLR_HOME_KEY, file.getCanonicalPath());
        } catch (Exception e) {
            throw new RuntimeException("初始化solr配置时出错了：", e);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

}
